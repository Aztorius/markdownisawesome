#!/bin/bash
# Usage : build_slides.sh file.md
pandoc -st beamer $1 -V theme:CambridgeUS -o slides.tex
pdflatex slides.tex
mv slides.pdf $1.pdf
