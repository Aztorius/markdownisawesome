# MarkdownIsAwesome

Convert Markdown to LateX and PDF for both presentations and reports.

## Dependencies

- pdflatex
- pandoc
