#!/bin/bash
# Usage : build_report.sh file.md
pandoc -s $1 -o report.tex
pdflatex report.tex
mv report.pdf $1.pdf
